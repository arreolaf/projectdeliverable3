
package uno;

public class Card {
	
	private static CardColor color;
	private static CardsValue value;
	
	Card(){
		CardColor.cardColors();
		CardsValue.cardValues();
	}

	public String toString(){	
		String out= "["+CardColor.cardColors();
		out+= " "+CardsValue.cardValues() + "]";
		return out;
	}
}
