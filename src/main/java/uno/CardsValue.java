
package uno;

import java.util.Random;


    public enum CardsValue {
    ZERO,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
	WILDCARD;
    
    
     static Random rand = new Random();
    
    public static String cardValues() {
        String result = "";
        switch(rand.nextInt(11)) {
        case 0:
            CardsValue zero = CardsValue.ZERO;
            result = "0";
            break;

        case 1:
            CardsValue one = CardsValue.ONE;
            result = "1";

            break;
        case 2:
            CardsValue two = CardsValue.TWO;
            result = "2";

            break;
        case 3:
            CardsValue three = CardsValue.THREE;
            result = "3";

            break;
        case 4:
            CardsValue four = CardsValue.FOUR;
            result = "4";

            break;
        case 5:
            CardsValue five = CardsValue.FIVE;
            result = "5";

            break;
        case 6:
            CardsValue six = CardsValue.SIX;
            result = "6";

            break;
        case 7:
            CardsValue seven = CardsValue.SEVEN;
            result = "7";

            break;
        case 8:
            CardsValue eight = CardsValue.EIGHT;
            result = "8";

            break;
        case 9:
            CardsValue nine = CardsValue.NINE;
            result = "9";

            break;
		case 10:
			CardsValue wildcard = CardsValue.WILDCARD;
			result = "WILDCARD";
			break;
            default:
            
        }
        return result;
    }
}

